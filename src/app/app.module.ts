import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';

import { AppRoutingModule } from './app-routing.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { OrdersModule } from './orders/orders.module';
import { PlataformaComponent } from './administrativo/plataforma/plataforma.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import * as $ from "jquery";
import { AppComponent } from './app.component';
import { LoginComponent } from './administrativo/login/login.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { NumbersOnlyDirective } from './directive/numbers-only.directive';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

//COMPONENTES
import {MatButtonModule} from '@angular/material/button';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    PlataformaComponent,
    NotFoundComponent,
    NumbersOnlyDirective
  ],
  imports: [
    BrowserModule,
    CommonModule,
    FontAwesomeModule,
    AppRoutingModule,
    OrdersModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgbModule,
    BrowserAnimationsModule,
    MatButtonModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  exports: [
    NumbersOnlyDirective,BrowserAnimationsModule,MatButtonModule
  ]
})
export class AppModule { }
