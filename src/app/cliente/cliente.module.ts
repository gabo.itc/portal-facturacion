import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { ClienteComponent } from './cliente.component';
import { ClienteRoutingModule } from './cliente-routing.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { FacturarComponent } from './facturar/facturar.component';
import { InicioComponent } from './inicio/inicio.component';

@NgModule({
  declarations: [
    ClienteComponent,
    FacturarComponent,
    InicioComponent,
  ],
  imports: [
    CommonModule,
    ClienteRoutingModule,
    FontAwesomeModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [ClienteComponent]
})
export class ClienteModule { }
