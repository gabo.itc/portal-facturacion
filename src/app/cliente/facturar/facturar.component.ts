import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import { environment } from 'src/environments/environment';
import { VentasService } from 'src/app/services/portal/ventas.service';
import { CatalogoFacturacionService } from 'src/app/services/facturacion/catalogo/catalogo.facturacion.service';

// import 'sweetalert2/src/sweetalert2.scss'
// import { CFDIConsultaService } from 'src/app/services/facturacion/cfdiconsulta.service';

@Component({
  selector: 'app-facturar',
  templateUrl: './facturar.component.html',
  styleUrls: ['./facturar.component.css']
})

export class FacturarComponent implements OnInit {

  formRevision: FormGroup = new FormGroup({
    folio: new FormControl(''),
    importe_total: new FormControl('', Validators.pattern(/^([0-9]+).([0-9]{2,6})$/)),
    rfc: new FormControl('', [Validators.pattern(/^([A-Z]{3,4})([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|[12][0-9]|3[01])([A-Z0-9]{3})$/)]),
    uso_cfdi: new FormControl(''),
    regimen_fiscal: new FormControl('')
  });
  formFacturacion: FormGroup = new FormGroup({
    razon_social: new FormControl(''),
    codigo_postal: new FormControl('', [Validators.pattern(/^([0-9]{5})$/)]),
    correo: new FormControl('')
  });

  dataForm: any = {};
  catalogo_usocfdi_descargado: any = [];
  catalogo_regimen_fiscal_descargado: any = [];
  catalogo_usocfdi: any = [];
  catalogo_regimen_fiscal: any = [];

  folioRevisado: boolean = false;

  conceptos: any = {};
  constructor(
    private formBuilder: FormBuilder,
    private ventasService: VentasService,
    private catalogoFacturacionService: CatalogoFacturacionService) {
      this.catalogoUsoCFDI();
      this.catalogoRegimenFiscal();
  }

  ngOnInit(): void {
    console.log("pruebas");
  }
  
  ngAfterContentInit(): void{
    this.formRevision.setValue({ folio: 'S2000010001', importe_total: 17400, rfc: 'EKU9003173C9', uso_cfdi: 'G03', regimen_fiscal: '601' });
    // this.formRevision.setValue({ folio: 'S2000010001', importe_total: 17400, rfc: 'EKU9003173C9', uso_cfdi: '', regimen_fiscal: '' });
    this.actualizarCatalogosByRFC(this.formRevision.value.rfc);
  }

  facturar(form: FormGroup): void {
    if (!form.invalid) {
      if (!this.folioRevisado) {       
        this.validarFolio(form);
      } else {
        this.solicitarFactura(form.value);
      }
    }
  }

  /**
   * Escuchador de cambios al perder el foco del txtRFC
   * */
  onChangeEventRFC(event: any) {
    this.actualizarCatalogosByRFC(event.target.value);
  }

  /**
   * Actualiza la información de los catalogos validando si el rfc pertenece a una persona moral o fisica 
   * */
  actualizarCatalogosByRFC(rfc: string) {

    let expresion_persona_moral = /^([A-Z]{3})([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|[12][0-9]|3[01])([A-Z0-9]{3})$/;
    let expresion_persona_fisica = /^([A-Z]{4})([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|[12][0-9]|3[01])([A-Z0-9]{3})$/;

    let es_persona_moral = expresion_persona_moral.test(rfc);
    let es_persona_fisica = expresion_persona_fisica.test(rfc);

    this.catalogo_usocfdi = this.catalogo_usocfdi_descargado.filter(uso_cfdi => (uso_cfdi.moral == 'S' && es_persona_moral) || (uso_cfdi.fisica == 'S' && es_persona_fisica));
    this.catalogo_regimen_fiscal = this.catalogo_regimen_fiscal_descargado.filter(uso_cfdi => (uso_cfdi.moral == 'S' && es_persona_moral) || (uso_cfdi.fisica == 'S' && es_persona_fisica));
  }

  validarFolio(form: FormGroup): void {
    // if (form.invalid) {
    //   console.log('Formulario Invalido');
    //   return;
    // }
    let dataForm = form.value;
    Swal.fire({
      title: 'Verificando Información',
      html: 'Validando..',// add html attribute if you want or remove
      allowOutsideClick: false,
      showConfirmButton: false,
      onBeforeOpen: () => {
        Swal.showLoading()
      },
    });
    this.folioRevisado = false;
    console.log("validando...")
    console.log(dataForm)
    this.ventasService.ventaConsultaFolio(dataForm).subscribe(success => {
      Swal.close();

      Object.assign(this.dataForm, dataForm);
      console.log("respuesta");
      console.log(success);
      this.conceptos = success.conceptos;
      if (success.conceptos.length > 0) {
        this.folioRevisado = true;
      }

      if (success.cliente != null) {
        this.formFacturacion.setValue(success.cliente);
      }

    }, error => {
      console.log('error: ');
      console.log(error);
      
      if (error.error.error != undefined) {
        Swal.fire({
          title: 'Validación',
          text: error.error.message,
          icon: 'warning',
          showConfirmButton: false,
          showCancelButton: true,
        });
      } else {
        Swal.fire({
          title: 'Verificando Información',
          text: 'No fue posible procesar la información',
          icon: 'error',
          showConfirmButton: false,
          showCancelButton: true,
        });
        console.error(error);
      }
    })
    // return this.folioRevisado;
  }

  solicitarFactura(dataForm: any): void {
    let data = this.dataForm;
    Object.assign(data, dataForm);
    console.log(data);
    // return;
    console.log("Solicitando Factura");
    Swal.fire({
      title: 'Solicitando Factura',
      html: 'Validando..',// add html attribute if you want or remove
      allowOutsideClick: false,
      showConfirmButton: false,
      onBeforeOpen: () => {
        Swal.showLoading()
      },
    });
    this.ventasService.ventaTimbrar(data).subscribe(
      success => {
        console.log(success);
        //this.catalogo_usocfdi = success;
      }, error => {
        console.error(error);
        if (error.error != undefined && error.error.error != undefined) {
          Swal.fire({
            title: error.error.error.type,
            text: error.error.error.message,
            icon: 'warning',
            showConfirmButton: false,
            showCancelButton: true,
          });
        } else {
          Swal.fire({
            title: 'Problemas con el Servidor',
            text: 'No fue posible procesar la información',
            icon: 'error',
            showConfirmButton: false,
            showCancelButton: true,
          });
        }
      }
    );
  }

  regresar(): void {
    this.folioRevisado = false;
  }

  catalogoUsoCFDI(): void {
    console.log("uso-cfdi");

    this.catalogoFacturacionService.usoCFDI({}).subscribe(
      success => {
        console.log(success);
        this.catalogo_usocfdi_descargado = success;
      }, error => {
        console.log(error);
      }
    );
  }

  catalogoRegimenFiscal(): void {
    console.log("regimen-fiscal");

    this.catalogoFacturacionService.regimenFiscal({}).subscribe(
      success => {
        console.log(success);
        this.catalogo_regimen_fiscal_descargado = success;
      }, error => {
        console.log(error);
      }
    );
  }
}
