import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ClienteComponent } from './cliente.component';
import { FacturarComponent } from './facturar/facturar.component';

// const routes: Routes = [{ path: '', component: ClienteComponent ,children:[]}];

const routes: Routes = [
    {
        path: '', component: ClienteComponent,
        // path: '', redirectTo:'facturar',
        children: [
            { path: 'facturar', component: FacturarComponent },
        //     // { path: '**', redirectTo: 'cliente', pathMatch: 'full' }

        ]
    }
    // ,{ path: 'facturados', component: FacturarComponent },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class ClienteRoutingModule { }
