import { Component, OnInit } from '@angular/core';
import { faDownload, IconDefinition } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-cliente',
  templateUrl: './cliente.component.html',
  styleUrls: ['./cliente.component.css']
})
export class ClienteComponent implements OnInit {
  faDownload: IconDefinition = faDownload;
  constructor() {
    // faDownload: faDownload;
  }

  ngOnInit(): void {
  }

}
