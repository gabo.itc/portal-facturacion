import { NgModule } from '@angular/core';
import { CommonModule, CurrencyPipe, UpperCasePipe } from '@angular/common';

import { AdministrativoRoutingModule } from './administrativo-routing.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { DataTablesModule } from 'angular-datatables';

import { AdministrativoComponent } from './administrativo.component';
import { NavbarComponent } from './navbar/navbar.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { PublicacionesComponent } from './publicaciones/publicaciones.component';
import { RegistroComponent } from './ventas/registro/registro.component';
import { VentasConsultaComponent } from './ventas/consulta/ventas-consulta.component';
import { LogoutComponent } from './login/logout/logout.component';
import { FacturacionConsultaComponent } from './facturacion/consulta/facturacion-consulta.component';

@NgModule({
  declarations: [
    AdministrativoComponent,
    NavbarComponent,
    PublicacionesComponent,
    SidebarComponent,
    RegistroComponent,
    VentasConsultaComponent,
    FacturacionConsultaComponent,
    LogoutComponent
  ],
  imports: [
    CommonModule,
    AdministrativoRoutingModule,
    FontAwesomeModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    DataTablesModule 
  ],
  exports: [
    AdministrativoRoutingModule,
    NavbarComponent,
    SidebarComponent
  ],
  providers: [CurrencyPipe],
  bootstrap: [AdministrativoComponent]
})
export class AdministrativoModule { }
