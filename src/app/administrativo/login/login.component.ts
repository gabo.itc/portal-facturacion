import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { User, WSPortalResponse } from 'src/app/models/user.model';
import { AuthenticationService } from 'src/app/services/portal/authentication.service';
import { StorageService } from 'src/app/services/portal/storage.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {
  public loginForm: FormGroup | any = {};
  public submitted?: Boolean;
  public mensaje_error: string = "";

  // public error: { code: number, message: string } = null;

  constructor(private formBuilder: FormBuilder,
    private authenticationService: AuthenticationService,
    private storageService: StorageService,
    private router: Router) {
    if (this.storageService.isAuthenticated()) {
      this.router.navigate(['administrativo'], {});
    }
  }

  ngOnInit(): void {
    const usuario_nombreValidators = [Validators.required, Validators.minLength(5)];
    const usuario_claveValidators = [Validators.required, Validators.minLength(5)];
    this.loginForm = this.formBuilder.group({
      usuario_nombre: ['', usuario_nombreValidators],
      usuario_clave: ['', usuario_claveValidators]
    });

  }

  public submitLogin(): void {
    console.log("logeandose..");
    this.mensaje_error = "";
    this.submitted = true;
    // this.error = null;
    if (this.loginForm.valid) {
      this.authenticationService.login(this.loginForm.value.usuario_nombre, this.loginForm.value.usuario_clave).subscribe(
        success => { }
        , error => {
          if (error.error) {
            this.mensaje_error = error.error.message;
          }
        }
        // ,error => this.error = JSON.parse(error._body)
      )
    }
  }

  private verificandoLogin() {
    this.router.navigate(['/administrativo']);
  }
}
