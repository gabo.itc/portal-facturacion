import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdministrativoComponent } from './administrativo.component';
import { PublicacionesComponent } from './publicaciones/publicaciones.component';
import { VentasConsultaComponent } from './ventas/consulta/ventas-consulta.component';
import { RegistroComponent } from './ventas/registro/registro.component';
import { PlataformaComponent } from './plataforma/plataforma.component';
import { AuthorizatedGuard } from '../guard/authorizated.guard';
import { FacturacionConsultaComponent } from './facturacion/consulta/facturacion-consulta.component';


// const routes: Routes = [{ path: '', component: ClienteComponent ,children:[]}];

const routes: Routes = [
    {
        path: '', component: AdministrativoComponent, canActivate:[AuthorizatedGuard], 
        children: [
            { path: 'publicaciones', component: PublicacionesComponent },
            { path: 'ventas/registrar', component: RegistroComponent },
            { path: 'ventas/consulta', component: VentasConsultaComponent },
            { path: 'plataforma', component: PlataformaComponent },
            { path: 'facturacion/consulta', component: FacturacionConsultaComponent },
            { path: 'facturacion/registro', component: FacturacionConsultaComponent },
        ]
    }
];

declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}
export const routesInfo: RouteInfo[] = [
    { path: '/dashboard', title: 'Dashboard',  icon: 'ni-tv-2 text-primary', class: '' },
    { path: '/icons', title: 'Icons',  icon:'ni-planet text-blue', class: '' },
    { path: '/maps', title: 'Maps',  icon:'ni-pin-3 text-orange', class: '' },
    { path: '/user-profile', title: 'User profile',  icon:'ni-single-02 text-yellow', class: '' },
    { path: '/tables', title: 'Tables',  icon:'ni-bullet-list-67 text-red', class: '' },
    { path: '/login', title: 'Login',  icon:'ni-key-25 text-info', class: '' },
    { path: '/register', title: 'Register',  icon:'ni-circle-08 text-pink', class: '' }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class AdministrativoRoutingModule { }
