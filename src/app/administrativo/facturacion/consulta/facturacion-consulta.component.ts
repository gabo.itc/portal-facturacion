import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { VentaModel } from 'src/app/models/venta.model';
import { FacturacionService } from 'src/app/services/portal/facturacion.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';
import { FacturaModel } from 'src/app/models/factura.model';
declare var $: any;

@Component({
  selector: 'app-facturacion-consulta',
  templateUrl: './facturacion-consulta.component.html',
  styleUrls: ['./facturacion-consulta.component.css']
})
export class FacturacionConsultaComponent implements OnInit {

  dtOptions: DataTables.Settings = {};
  anio_actual: number = new Date().getFullYear();

  /*Variables para consulta*/
  tipo_filtro: string = "";

  serie: string = "";
  folio: string = "";

  fecha_inicial: string = "";
  fecha_final: string = "";

  ejercicio: number = this.anio_actual;
  mes?: string;

  lista_facturas: FacturaModel[] = [];

  /*Variables para procesamiento*/
  contenidoXML: string = "";

  constructor(
    private facturacionService: FacturacionService,
    private ngbModal: NgbModal
  ) { }

  ngOnInit(): void {
    console.log(new Date());

    this.dtOptions = {
      // data: this.ventas_por_ticket,
      info: true, /*pie de pagina con informacion*/
      paging: false, /*paginacion */
      // pagingType: 'full_numbers',
      // pageLength: 10,
      processing: true,
      language: {
        emptyTable: '',
        zeroRecords: 'No hay coincidencias',
        lengthMenu: 'Mostrar _MENU_ elementos',
        search: 'Buscar:',
        info: 'De _START_ a _END_ de _TOTAL_ elementos',
        infoEmpty: 'De 0 a 0 de 0 elementos',
        infoFiltered: '(filtrados de _MAX_ elementos totales)',
        paginate: {
          first: 'Prim.',
          last: 'Últ.',
          next: 'Sig.',
          previous: 'Ant.'
        },
      },
      lengthChange: false, /*combo para numero de registros visibles*/
      searching: false, /*txt para busqueda de elementos*/
      ordering: true, /*habilitamos la ordenacion*/
      columnDefs: [{ orderable: false, targets: [0] }], // to disable columns order, may cause error if not put correctly
      order: [[2, 'asc']],
    };
  }

  consulta(datos: NgForm): void {
    this.lista_facturas = [];
    Swal.fire({
      title: 'Consultando Información',
      text: 'Consultando..',// add html attribute if you want or remove
      allowOutsideClick: false,
      showConfirmButton: false,
      onBeforeOpen: () => {
        Swal.showLoading()
      },
    });
    console.log(datos.value);
    let data = datos.value;
    this.facturacionService.consultaFiltro(data).subscribe(data_success => {
      this.lista_facturas = data_success;

      if (data_success.length == 0) {
        Swal.fire({
          title: 'Información No Encontrada',
          text: 'Sin Información para Mostrar ',
          icon: 'info',
          showConfirmButton: true,
          showCancelButton: false,
        });
      } else {
        Swal.close();//cerramos el modal de carga
      }

    }, data_error => {
      console.log(data_error);
      if (typeof (data_error.error.message) !== 'undefined') {
        Swal.fire({
          title: 'Validación',
          text: data_error.error.message,
          icon: 'warning',
          showConfirmButton: true,
          showCancelButton: false,
        });
      } else {
        Swal.fire({
          title: 'Verificando Información',
          text: 'No fue posible procesar la información',
          icon: 'error',
          showConfirmButton: false,
          showCancelButton: true,
        });
      }
    })
  }

  verXML(modal: any, xml: string): void {
    this.ngbModal.open(modal, { size: 'xl', scrollable: true});

    let codigoHTML = "";
    codigoHTML += "<div style='color: rgb(153, 69, 0); '>";
    codigoHTML += xml.split("<").join("&lt;br&gt;\n&lt;").split(">").join("&gt;");
    codigoHTML = codigoHTML.split("\"?").join("</span>&quot;?");/*reemplazamos el cierre de comillas*/
    codigoHTML = codigoHTML.split("\"&gt;").join("</span>&quot;&gt;");/*reemplazamos el cierre de comillas*/
    codigoHTML = codigoHTML.split("\" ").join("</span>&quot; ");/*reemplazamos el cierre de comillas*/
    codigoHTML = codigoHTML.split("=\"").join("=&quot;<span style='color: rgb(26, 26, 166);'>");
    codigoHTML = codigoHTML.replace("&lt;br&gt;", "");/*eliminamos el primer salto de linea*/
    codigoHTML = codigoHTML.split("&lt;br&gt;").join("<br>");
    codigoHTML += "</div><br>";

    $(".modal-body").html(codigoHTML);
    return;
  }

  verPDF(modal: any, xml: string): void {
    this.ngbModal.open(modal, { size: 'xl', scrollable: true, windowClass: 'dark-modal' });
    this.contenidoXML = xml
    return;
  }

  cancelarFactura(factura: FacturaModel): void {
    Swal.fire({
      title: '¿Esta Seguro de Cancelar la Factura?',
      text: "el proceso no podra revertirse",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, Continuar!',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.isConfirmed) {

        this.facturacionService.facturaCancelar(factura).subscribe(data_success => {
          Swal.fire(
            'Factura Cancelada',
            'La Factura ha Sido Cancelada Por Completo',
            'success'
          )
          let indice = -1;
          this.lista_facturas.forEach(venta => {
            if (venta == factura) {
              indice = this.lista_facturas.indexOf(venta); // obtenemos el indice
            }
          });
          this.lista_facturas.splice(indice, 1); // 1 es la cantidad de elemento a eliminar

        }, data_error => {
          console.log(data_error);
          if (typeof (data_error.error.message) !== 'undefined') {
            Swal.fire({
              title: 'Validación',
              text: data_error.error.message,
              icon: 'warning',
              showConfirmButton: true,
              showCancelButton: false,
            });
          } else {
            Swal.fire({
              title: 'Verificando Información',
              text: 'No fue posible procesar la información',
              icon: 'error',
              showConfirmButton: false,
              showCancelButton: true,
            });
          }
        })
      }
    })
  }
}
