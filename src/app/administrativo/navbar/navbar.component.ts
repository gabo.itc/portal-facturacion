import { Component, OnInit, ElementRef } from '@angular/core';
import { Location, LocationStrategy, PathLocationStrategy } from '@angular/common';
import { Router } from '@angular/router';
import { routesInfo } from '../administrativo-routing.module';
import { StorageService } from 'src/app/services/portal/storage.service';
// import { routes } from '../administrativo.module';
// import { routes } from './administrativo.module';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  public location: Location;
  public focus = false;
  public listTitles: any[] = [];
  public usuario_nombre: string;

  constructor(location: Location,
    private element: ElementRef,
    private storage: StorageService,
    private router: Router) {
    this.location = location;
    this.usuario_nombre = <string>this.storage.getCurrentUser();
  }

  ngOnInit(): void {
    this.listTitles = routesInfo.filter(listTitle => listTitle);
  }

  getTitle() {
    var titlee = this.location.prepareExternalUrl(this.location.path());
    if (titlee.charAt(0) === '#') {
      titlee = titlee.slice(1);
    }

    for (var item = 0; item < this.listTitles.length; item++) {
      if (this.listTitles[item].path === titlee) {
        return this.listTitles[item].title;
      }
    }
    return 'Inicio';
  }

}
