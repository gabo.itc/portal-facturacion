import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-plataforma',
  templateUrl: './plataforma.component.html',
  styleUrls: ['./plataforma.component.css']
})
export class PlataformaComponent implements OnInit {
  Descripcion: String = "";
  Estatus: boolean = false;
  pk_plataforma: number = 0;

  Plataformas: any = {};

  Agregar_Plataforma: boolean = true;
  constructor() { }

  ngOnInit(): void {
   
  }

  Plataforma(datos: NgForm): void {
    console.log(datos);
    if (datos.invalid) {
      console.log('Formulario Invalido');
      return;
    } else {
      this.Validar_Plataforma(); 
    }
  }

  Validar_Plataforma(): void {
    Swal.fire({
      title: 'Verificando Información',
      html: 'Validando..',// add html attribute if you want or remove
      allowOutsideClick: false,
      showConfirmButton: false,
      onBeforeOpen: () => {
        Swal.showLoading()
      },
    });
    this.Agregar_Plataforma = false;
    let data = {
      //folio: this.folio,
      //importe: this.total
    }
    console.log("validando...")
    /*this.ventasService.consultaFolio(data).subscribe(success => {
      console.log(success);
      console.log(success.message);
      if (typeof (success.message.Conceptos) === 'undefined') {
        Swal.fire({
          title: 'Información No Encontrada',
          text: 'No. de Ticket/ Importe Incorrecto ',
          icon: 'error',
          showConfirmButton: false,
          showCancelButton: true,
        });

      } else {
        Swal.close();
        if (success.message.Conceptos.length > 0) {
          this.conceptos = success.message.Conceptos;
          console.log("respuesta");
          console.log(this.conceptos);
          this.Agregar_Plataforma = true;
          this.catalogoUsoCFDI();
        }

      }
    }, error => {
      Swal.close();
      Swal.fire({
        title: 'Verificando Información',
        // html: 'Validando..',// add html attribute if you want or remove
        text: 'No fue posible procesar la información',
        icon: 'warning',
        showConfirmButton: false,
        showCancelButton: true,
      });
      console.error(error);
    })*/
    // return this.folioRevisado;
  }
}
