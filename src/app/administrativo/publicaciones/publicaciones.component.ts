import { Component, OnInit } from '@angular/core';
import { VentasService } from 'src/app/services/portal/ventas.service';
import { CatalogoSATService } from 'src/app/services/portal/catalogosat.service';
import Swal from 'sweetalert2';
import { ClaveProdServModel, ClaveUnidadModel } from 'src/app/models/catalogosat';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';
import { Observable } from 'rxjs';
import { CatalogoPortalService } from 'src/app/services/portal/catalogo.portal.service';
import { PlataformaModel } from 'src/app/models/plataforma.model';
import { ConceptoRegistroModel } from 'src/app/models/conceptos.interface';
import { FormaPagoModel } from 'src/app/models/formapago.model';
import { CatalogoFacturacionService } from 'src/app/services/facturacion/catalogo/catalogo.facturacion.service';
import { NgForm } from '@angular/forms';
import { PublicacionRegistroModel } from 'src/app/models/publicaciones.interface';
import { PublicacionesService } from 'src/app/services/portal/publicaciones.service';


@Component({
  selector: 'app-publicaciones',
  templateUrl: './publicaciones.component.html',
  styleUrls: ['./publicaciones.component.css']
})

export class PublicacionesComponent implements OnInit {
  keyword = "descripcion";
  data = [{ descripcion: "uno" }, { descripcion: "dos" }, { descripcion: "tres" }];
  autocomplete_notfound = "Sin Coincidencias";

  catalogo_plataforma: PlataformaModel[] = [];
  catalogo_claves_unidad: ClaveUnidadModel[] = [];
  catalogo_claves_productos_servicios: ClaveProdServModel[] = [];
  registro_publicaciones: PublicacionRegistroModel[] = [];

  fecha_publicacion: string = "";
  plataforma: string = null;
  posicion: number = -1;
  titulo: string = "";
  identificador: string = "";
  unidad: string = "";
  sat_clave_unidad: string = "";
  descripcion_claveprodserv: string = "";
  sat_claveprodserv: string = "";
  aplica_iva: boolean = true;
  incluye_iva: boolean = false;
  tasa_iva: number = 0.16000;
  precio_unitario: number = 0;
  importe_iva: number = 0;//((this.cantidad*this.precio_unitario)-this.descuento)*(this.aplica_iva ? this.tasa_iva : 0);
  importe: number = 0;//this.cantidad * this.precio_unitario;
  link: string = "";
  // importe: number = 0;

  folio: string = "";

  constructor(private catalogoPortalService: CatalogoPortalService,
    private catalogoFacturacionService: CatalogoFacturacionService,
    private publicacionService: PublicacionesService,
    private catalogoSATService: CatalogoSATService) {
    this.limpiar();
    // this.data$ = this.catalogoSATService.producto_servicio({});
  }

  ngOnInit(): void {
    Swal.fire({
      title: 'Cargando Catalogos',
      showConfirmButton: false,
      showCancelButton: false,
      onBeforeOpen: () => {
        Swal.showLoading()
      },
    });

    this.catalogoPortalService.tipoPlataforma({}).subscribe(success => {
      this.catalogo_plataforma = success.data;
    }, error => { console.error(error); });

    this.catalogoSATService.unidad({}).subscribe(success => {
      this.catalogo_claves_unidad = success;
    }, error => { console.error(error); });

    this.catalogoSATService.producto_servicio({}).subscribe(success => {
      this.catalogo_claves_productos_servicios = success;
      Swal.close();
    }, error => { Swal.close(); console.error(error); });
  }

  getTotal(campo: string): number {
    let registro: any[] = this.registro_publicaciones;
    return registro.map(venta => venta[campo]).reduce((a, b) => a + b, 0);;
  }

  agregar(form: NgForm): void {
    if (!form.valid) {
      console.log("invalido");
      return;
    }

    let importe_sin_iva = 0;
    let importe = (this.precio_unitario);
    let importe_iva = 0;
    let precio_unitario = this.precio_unitario;
    if (this.aplica_iva) {
      if (this.incluye_iva) {
        importe = (this.precio_unitario);
        importe_sin_iva = Number((importe / (1 + this.tasa_iva)).toFixed(2));
        importe_iva = importe - importe_sin_iva;
        precio_unitario = Number((importe_sin_iva).toFixed(2));
      } else {
        importe_sin_iva = (this.precio_unitario);
        importe = Number((importe_sin_iva * (1 + this.tasa_iva)).toFixed(2));
        importe_iva = importe - importe_sin_iva;
      }
      // importe_iva = 0;
    }

    let publicacion: PublicacionRegistroModel = {
      fecha_publicacion: this.fecha_publicacion,
      plataforma_acronimo: this.plataforma,
      titulo: this.titulo,
      identificador: this.identificador,
      unidad: this.unidad,
      sat_clave_unidad: this.sat_clave_unidad,
      sat_claveprodserv: this.sat_claveprodserv,
      precio_unitario: precio_unitario,
      importe: importe_sin_iva,
      incluye_iva: this.incluye_iva,
      aplica_iva: this.aplica_iva,
      tasa_iva: this.tasa_iva,
      importe_iva: importe_iva,
      link: this.link
    }
    if (this.posicion == -1) {/*agregara un nuevo registro*/
      this.registro_publicaciones.push(publicacion);
    } else {
      this.registro_publicaciones[this.posicion] = publicacion;
    }
    // form.resetForm();
    this.limpiar();
  }

  limpiar(): void {
    this.posicion = -1;
    this.fecha_publicacion = "";
    this.plataforma = undefined;
    this.titulo = "asdasdas";
    this.identificador = "01";
    this.unidad = "E48";
    this.sat_clave_unidad = "E48";
    this.descripcion_claveprodserv = "111111";
    this.sat_claveprodserv = "01010101";
    this.aplica_iva = true;
    this.incluye_iva = true;
    this.tasa_iva = 0.16000;
    this.precio_unitario = 17400;
    this.importe_iva = 0;
    this.importe = 17400;
  }

  eliminar(posicion: number): void {
    this.registro_publicaciones.splice(posicion, 1);
  }

  modificar(posicion: number): void {
    this.limpiar();
    let publicacion: PublicacionRegistroModel = this.registro_publicaciones[posicion];

    this.posicion = posicion;
    this.fecha_publicacion = publicacion.fecha_publicacion;
    this.plataforma = publicacion.plataforma_acronimo;
    this.titulo = publicacion.titulo;
    this.identificador = publicacion.identificador;
    this.unidad = publicacion.unidad;
    this.sat_clave_unidad = publicacion.sat_clave_unidad;
    this.sat_claveprodserv = publicacion.sat_claveprodserv;
    this.aplica_iva = publicacion.aplica_iva;
    this.incluye_iva = publicacion.incluye_iva;
    this.tasa_iva = publicacion.tasa_iva;
    this.precio_unitario = publicacion.precio_unitario;
    this.importe_iva = publicacion.importe_iva;
    this.importe = publicacion.importe;
    if (this.incluye_iva) {
      this.importe += this.importe_iva;
      this.precio_unitario += this.importe_iva;
    }
    console.log(publicacion);

  }

  registrar(): void {
    if (this.titulo == '') {
      Swal.fire({
        title: 'Validación',
        html: "Ingrese el Titulo de la Publicación",
        icon: 'warning',
        showConfirmButton: true,
      });
      return;
    }
    Swal.fire({
      title: 'Capturando',
      html: 'Subiendo Información..',// add html attribute if you want or remove
      allowOutsideClick: false,
      showConfirmButton: false,
      onBeforeOpen: () => {
        Swal.showLoading()
      },
    });

    let data = {
      publicaciones: this.registro_publicaciones
    };

    console.log(JSON.stringify(data));
    return;

    this.publicacionService.publicacionRegistro(data).subscribe(success => {
      let showMensaje = false;
      let mensaje = {};
      let titulo = "";
      let text = "";
      let icon = "";
      if (typeof (success.error) !== 'undefined' && success.error == true) {
        Swal.fire({
          title: 'Validación',
          text: success.message,
          icon: 'warning',
          showConfirmButton: true,
          showCancelButton: false,
        });
      } else {

        console.log(success);
        if (typeof (success.body.folio_venta) !== 'undefined') {
          Swal.fire({
            title: 'Venta Capturada',
            html: 'Folio: <b>' + success.body.folio_venta + "</b>",
            icon: 'info',
            showConfirmButton: true,
            showCancelButton: false,
          });
          this.limpiar();

          this.registro_publicaciones = [];
        } else {
          Swal.fire({
            title: 'Venta Generada',
            text: 'La Venta fue generada pero no fue posible recuperar el numero de folio',
            icon: 'warning',
            showConfirmButton: false,
            showCancelButton: true,
          });
        }
      }
    }, error => {
      Swal.close();
      Swal.fire({
        title: 'Verificando Información',
        // html: 'Validando..',// add html attribute if you want or remove
        text: 'No fue posible procesar la información',
        icon: 'error',
        showConfirmButton: false,
        showCancelButton: true,
      });
      console.error(error);
    })
  }

  getClaveUnidadFiltrado(): ClaveUnidadModel[] {
    let catalogo_claves_unidad: ClaveUnidadModel[] = [];
    if (this.sat_clave_unidad.length >= 1) {
      this.catalogo_claves_unidad.forEach(unidad => {
        if (unidad.descripcion != null && unidad.clave_unidad != null) {
          let valor_compara = this.sat_clave_unidad.toLocaleLowerCase();
          if (unidad.clave_unidad.toLocaleLowerCase().includes(valor_compara) || unidad.descripcion.toLocaleLowerCase().includes(valor_compara)) {
            if (catalogo_claves_unidad.length <= 50) {
              catalogo_claves_unidad.push(unidad);
            }
          }
        }
      });
    }
    return catalogo_claves_unidad;

  }
  getClaveProductoervicioFiltrado(): ClaveProdServModel[] {
    let catalogo_claves_productos_servicios: ClaveProdServModel[] = [];
    if (this.sat_claveprodserv.length >= 3) {
      this.catalogo_claves_productos_servicios.forEach(productoservicio => {
        if (productoservicio.descripcion != null && productoservicio.clave_producto_servicio != null) {

          let valor_compara = this.sat_claveprodserv.toLocaleLowerCase();
          let campo_filtro = productoservicio.clave_producto_servicio.toLocaleLowerCase();

          if (isNaN(Number(this.sat_claveprodserv))) {
            campo_filtro = productoservicio.descripcion.toLocaleLowerCase();
          }
          if (campo_filtro.includes(valor_compara)) {
            if (catalogo_claves_productos_servicios.length <= 50) {
              catalogo_claves_productos_servicios.push(productoservicio);
            }
          }
        }
      });
    }
    return catalogo_claves_productos_servicios;

  }
}
