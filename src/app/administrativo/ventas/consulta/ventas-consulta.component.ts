import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { VentaModel } from 'src/app/models/venta.model';
import { VentasService } from 'src/app/services/portal/ventas.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';
declare var $: any;

@Component({
  selector: 'app-ventas-consulta',
  templateUrl: './ventas-consulta.component.html',
  styleUrls: ['./ventas-consulta.component.css']
})
export class VentasConsultaComponent implements OnInit {

  dtOptions: DataTables.Settings = {};
  folio: string = "";
  fecha_inicial: string = "";
  fecha_final: string = "";
  tipo_filtro: string = "";
  ventas_general: VentaModel[] = [];
  ventas_agrupado_ticket: VentaModel[] = [];
  ventas_por_ticket: VentaModel[] = [];
  ventas_por_ticket_folio: string = "";

  constructor(
    private ventasService: VentasService,
    private ngbModal: NgbModal
  ) { }

  ngOnInit(): void {
    this.dtOptions = {
      // data: this.ventas_por_ticket,
      info: true, /*pie de pagina con informacion*/
      paging: false, /*paginacion */
      // pagingType: 'full_numbers',
      // pageLength: 10,
      processing: true,
      language: {
        emptyTable: '',
        zeroRecords: 'No hay coincidencias',
        lengthMenu: 'Mostrar _MENU_ elementos',
        search: 'Buscar:',
        info: 'De _START_ a _END_ de _TOTAL_ elementos',
        infoEmpty: 'De 0 a 0 de 0 elementos',
        infoFiltered: '(filtrados de _MAX_ elementos totales)',
        paginate: {
          first: 'Prim.',
          last: 'Últ.',
          next: 'Sig.',
          previous: 'Ant.'
        },
      },
      lengthChange: false, /*combo para numero de registros visibles*/
      searching: false, /*txt para busqueda de elementos*/
      ordering: true, /*habilitamos la ordenacion*/
      columnDefs: [{ orderable: false, targets: [0] }], // to disable columns order, may cause error if not put correctly
      order: [[2, 'asc']],
    };
  }

  consulta(datos: NgForm): void {
    this.ventas_general = [];
    this.ventas_agrupado_ticket = [];
    this.ventas_por_ticket = [];
    Swal.fire({
      title: 'Consultando Información',
      text: 'Consultando..',// add html attribute if you want or remove
      allowOutsideClick: false,
      showConfirmButton: false,
      onBeforeOpen: () => {
        Swal.showLoading()
      },
    });
    console.log(datos.value);
    let data = datos.value;
    this.ventasService.ventaConsultaFiltro(data).subscribe(data_success => {
      console.log(data_success);
      
      this.ventas_general = data_success;

      if (data_success == null || data_success.length == 0) {
        Swal.fire({
          title: 'Información No Encontrada',
          text: 'Sin Información para Mostrar ',
          icon: 'info',
          showConfirmButton: true,
          showCancelButton: false,
        });
      } else {
        this.ventas_general.forEach(venta => {
          let ventas_filtradas = this.ventas_agrupado_ticket.filter((venta_ticket) => venta_ticket.folio_venta === venta.folio_venta);
          if (ventas_filtradas.length == 0) {
            this.ventas_agrupado_ticket.push(venta);
          } else {
            this.ventas_agrupado_ticket.filter((venta_ticket) => venta_ticket.folio_venta === venta.folio_venta)
              .forEach(venta_ticket => {
                venta_ticket.precio_unitario = Number(venta_ticket.precio_unitario) + Number(venta.precio_unitario);//venta_ticket.precio_unitario + venta.precio_unitario;
                venta_ticket.importe_venta = Number(venta_ticket.importe_venta) + Number(venta.importe_venta);//venta_ticket.precio_unitario + venta.precio_unitario;
                venta_ticket.descuento = Number(venta_ticket.descuento) + Number(venta.descuento);//venta_ticket.precio_unitario + venta.precio_unitario;
                venta_ticket.importe_iva = Number(venta_ticket.importe_iva) + Number(venta.importe_iva);//venta_ticket.precio_unitario + venta.precio_unitario;
                venta_ticket.importe_total = Number(venta_ticket.importe_total) + Number(venta.importe_total);//venta_ticket.precio_unitario + venta.precio_unitario;
              });
          }
        });
        Swal.close();//cerramos el modal de carga
      }

    }, data_error => {
      console.log(data_error);
      if (typeof (data_error.error.message) !== 'undefined') {
        Swal.fire({
          title: 'Validación',
          text: data_error.error.message,
          icon: 'warning',
          showConfirmButton: true,
          showCancelButton: false,
        });
      } else {
        Swal.fire({
          title: 'Verificando Información',
          text: 'No fue posible procesar la información',
          icon: 'error',
          showConfirmButton: false,
          showCancelButton: true,
        });
      }
    })
  }

  mostrarDetalles(modal: any, folio_venta: string): void {
    this.ngbModal.open(modal, { size: 'xl', windowClass: 'dark-modal' });
    this.ventas_por_ticket_folio = folio_venta;
    this.ventas_por_ticket = this.ventas_general.filter((venta_ticket) => venta_ticket.folio_venta === folio_venta);
    // this.ventas_por_ticket = this.ventas_general;
    return;
  }


  cancelarMovimientoVenta(id: string, id_venta: string): void {
    Swal.fire({
      title: '¿Esta Seguro de Cancelar el Movimiento?',
      text: "el proceso no podra revertirse",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, Continuar!',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire({
          text: 'Cancelando..',// add html attribute if you want or remove
          allowOutsideClick: false,
          showConfirmButton: false,
          onBeforeOpen: () => {
            Swal.showLoading()
          }
        });
        this.ventasService.ventaCancelarConcepto(id, id_venta).subscribe(data_success => {
          Swal.fire(
            'Movimiento Cancelado',
            'el Movimiento de la Venta ha Sido Cancelada con Exito',
            'success'
          )

          let indice = -1;
          this.ventas_por_ticket.forEach(venta => {
            if (venta.id_venta == id_venta) {
              indice = this.ventas_por_ticket.indexOf(venta); // obtenemos el indice
            }
          });
          this.ventas_por_ticket.splice(indice, 1); // 1 es la cantidad de elemento a eliminar

        }, data_error => {
          console.log(data_error);
          if (typeof (data_error.error.message) !== 'undefined') {
            Swal.fire({
              title: 'Validación',
              text: data_error.error.message,
              icon: 'warning',
              showConfirmButton: true,
              showCancelButton: false,
            });
          } else {
            Swal.fire({
              title: 'Verificando Información',
              text: 'No fue posible procesar la información',
              icon: 'error',
              showConfirmButton: false,
              showCancelButton: true,
            });
          }
        })
      }
    })
  }

  cancelarVenta(id: string): void {
    Swal.fire({
      title: '¿Esta Seguro de Cancelar el Folio?',
      text: "el proceso no podra revertirse",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, Continuar!',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.isConfirmed) {
        if (id != "") {
          this.ventasService.ventaCancelar(id).subscribe(data_success => {
            Swal.fire(
              'Venta Cancelada',
              'La Venta ha Sido Cancelada Por Completo',
              'success'
            )
            let indice = -1;
            this.ventas_agrupado_ticket.forEach(venta => {
              if (venta.folio_venta == id) {
                indice = this.ventas_agrupado_ticket.indexOf(venta); // obtenemos el indice
              }
            });
            this.ventas_agrupado_ticket.splice(indice, 1); // 1 es la cantidad de elemento a eliminar
  
          }, data_error => {
            console.log(data_error);
            if (typeof (data_error.error.message) !== 'undefined') {
              Swal.fire({
                title: 'Validación',
                text: data_error.error.message,
                icon: 'warning',
                showConfirmButton: true,
                showCancelButton: false,
              });
            } else {
              Swal.fire({
                title: 'Verificando Información',
                text: 'No fue posible procesar la información',
                icon: 'error',
                showConfirmButton: false,
                showCancelButton: true,
              });
            }
          })
        }
      }
    })
  }
}
