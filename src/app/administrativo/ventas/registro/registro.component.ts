import { Component, OnInit } from '@angular/core';
import { VentasService } from 'src/app/services/portal/ventas.service';
import { CatalogoSATService } from 'src/app/services/portal/catalogosat.service';
import Swal from 'sweetalert2';
import { ClaveProdServModel, ClaveUnidadModel } from 'src/app/models/catalogosat';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';
import { Observable } from 'rxjs';
import { CatalogoPortalService } from 'src/app/services/portal/catalogo.portal.service';
import { TipoVentaModel } from 'src/app/models/tipoventa.model';
import { ConceptoRegistroModel } from 'src/app/models/conceptos.interface';
import { FormaPagoModel } from 'src/app/models/formapago.model';
import { CatalogoFacturacionService } from 'src/app/services/facturacion/catalogo/catalogo.facturacion.service';
import { FormBuilder, FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { FuncionesService } from 'src/app/services/funciones.service';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {
  keyword = "descripcion";
  data = [{ descripcion: "uno" }, { descripcion: "dos" }, { descripcion: "tres" }];
  autocomplete_notfound = "Sin Coincidencias";

  catalogo_tipo_venta: TipoVentaModel[] = [];
  catalogo_forma_pago: FormaPagoModel[] = [];
  catalogo_claves_unidad: ClaveUnidadModel[] = [];
  catalogo_claves_productos_servicios: ClaveProdServModel[] = [];
  registro_ventas: ConceptoRegistroModel[] = [];



  form_registro_venta: FormGroup = new FormGroup({
    // importe_total: new FormControl('', Validators.pattern(/^([0-9]+).([0-9]{2,6})$/)),
    sat_forma_pago: new FormControl('', Validators.required),
    cantidad: new FormControl('',Validators.required),
    identificador: new FormControl(''),
    concepto: new FormControl('',[Validators.required]),
    unidad: new FormControl('',Validators.pattern(/^[^|]{1,20}$/)),
    sat_clave_unidad: new FormControl(''),
    sat_claveprodserv: new FormControl('',Validators.pattern(/^([0-9]+).([0-9]{2,6})$/)),
    precio_unitario: new FormControl(''),
    aplica_iva: new FormControl(''),
    incluye_iva: new FormControl(''),
    descuento: new FormControl(''),
    importe: new FormControl(''),
  });

  tasa_iva: number = 0.16000;

  tipo_venta: string = "";
  sat_forma_pago: string = "";
  fecha_venta: string = "";

  posicion: number = -1;

  folio: string = "";

  constructor(private catalogoPortalService: CatalogoPortalService,
    private catalogoFacturacionService: CatalogoFacturacionService,
    private ventasService: VentasService,
    private catalogoSATService: CatalogoSATService, private funcionesServ: FuncionesService) {
    this.limpiarVenta();
    // this.data$ = this.catalogoSATService.producto_servicio({});
  }

  ngOnInit(): void {
    Swal.fire({
      title: 'Cargando catalogos',
      showConfirmButton: false,
      showCancelButton: false,
      onBeforeOpen: () => {
        Swal.showLoading()
      },
    });

    this.catalogoPortalService.tipoVenta({}).subscribe(success => {
      this.catalogo_tipo_venta = success;
    }, error => { console.error(error); });

    this.catalogoSATService.formaPago({}).subscribe(success => {
      this.catalogo_forma_pago = success;
    }, error => { console.error(error); });

    this.catalogoSATService.unidad({}).subscribe(success => {
      this.catalogo_claves_unidad = success;
    }, error => { console.error(error); });

    this.catalogoSATService.producto_servicio({}).subscribe(success => {
      this.catalogo_claves_productos_servicios = success;
      Swal.close();
    }, error => { Swal.close(); console.error(error); });
  }

  ngAfterContentInit(): void {
    this.form_registro_venta.setValue({ sat_forma_pago: '01', cantidad: 1, concepto: "asdasdas", identificador: "01", unidad: "E48", sat_clave_unidad: "E48", sat_claveprodserv: "01010101", aplica_iva: true, incluye_iva: true, precio_unitario: 17400, descuento: 0, importe: 17400 });
  }

  getTotal(campo: string): number {
    let registro: any[] = this.registro_ventas;
    return registro.map(venta => venta[campo]).reduce((a, b) => a + b, 0);
  }

  agregarVenta(form: FormGroup): void {
    if (form.invalid) {
      console.log("invalido");
      return;
    }

    let importe_sin_iva = 0;
    let importe = (this.form_registro_venta.value.cantidad * this.form_registro_venta.value.precio_unitario) - (this.form_registro_venta.value.incluye_iva ? 0 : this.form_registro_venta.value.descuento);
    let importe_iva = 0;
    let precio_unitario = this.form_registro_venta.value.precio_unitario;
    let sat_objeto_impuesto = '02';
    if (this.form_registro_venta.value.aplica_iva) {
      sat_objeto_impuesto = '01';//si es objeto de impuesto
      if (this.form_registro_venta.value.incluye_iva) {
        importe = (this.form_registro_venta.value.cantidad * this.form_registro_venta.value.precio_unitario);
        importe_sin_iva = Number((importe / (1 + this.tasa_iva)).toFixed(2));
        importe_iva = importe - importe_sin_iva;
        precio_unitario = Number((importe_sin_iva / this.form_registro_venta.value.cantidad).toFixed(2));
      } else {
        importe_sin_iva = (this.form_registro_venta.value.cantidad * this.form_registro_venta.value.precio_unitario);
        importe = Number((importe_sin_iva * (1 + this.tasa_iva)).toFixed(2));
        importe_iva = importe - importe_sin_iva;
      }
      // importe_iva = 0;
    }

    let venta: ConceptoRegistroModel = this.form_registro_venta.value
    /*actualizacion de valores*/
    venta.sat_objeto_impuesto = sat_objeto_impuesto;
    venta.precio_unitario = precio_unitario;
    venta.importe = importe_sin_iva;
    venta.tasa_iva = this.tasa_iva;
    venta.importe_iva = importe_iva;
    if (this.posicion == -1) {/*agregara un nuevo registro*/
      this.registro_ventas.push(venta);
    } else {
      this.registro_ventas[this.posicion] = venta;
    }
    // this.form_registro_venta.clearValidators();
    form.reset(this.form_registro_venta);
    this.limpiarVenta();
  }

  limpiarVenta(): void {
    this.form_registro_venta.reset();
    // for(var name in this.form_registro_venta.controls) {
    //   //this.form_registro_venta.controls[name].setValue('');
    //   /*(<FormControl>form.controls[name]).updateValue('');*/ /*this should work in RC4 if `Control` is not working, working same in my case*/
    //   this.form_registro_venta.controls[name].setErrors(null);
    // }
  }

  eliminarVenta(posicion: number): void {
    this.registro_ventas.splice(posicion, 1);
  }

  modificarVenta(posicion: number): void {
    this.limpiarVenta();
    let venta: ConceptoRegistroModel = this.registro_ventas[posicion];

    this.posicion = posicion;
    this.form_registro_venta.setValue(venta);
    console.log(venta);

  }

  registrarVenta(): void {
    if (this.fecha_venta == '') {
      Swal.fire({
        title: 'Validación',
        html: "Ingrese la Fecha de Venta",
        icon: 'warning',
        showConfirmButton: true,
      });
      return;
    }
    Swal.fire({
      title: 'Capturando',
      html: 'Subiendo Información..',// add html attribute if you want or remove
      allowOutsideClick: false,
      showConfirmButton: false,
      onBeforeOpen: () => {
        Swal.showLoading()
      },
    });

    let data = {
      fecha_venta: this.fecha_venta,
      tipo_venta: this.tipo_venta,
      forma_pago: this.sat_forma_pago,
      ventas: this.registro_ventas
    };

    this.ventasService.ventaRegistro(data).subscribe(success => {
      let showMensaje = false;
      let mensaje = {};
      let titulo = "";
      let text = "";
      let icon = "";
      if (typeof (success.error) !== 'undefined' && success.error == true) {
        Swal.fire({
          title: 'Validación',
          text: success.message,
          icon: 'warning',
          showConfirmButton: true,
          showCancelButton: false,
        });
      } else {

        console.log(success);
        if (typeof (success.body.folio_venta) !== 'undefined') {
          Swal.fire({
            title: 'Venta Capturada',
            html: 'Folio: <b>' + success.body.folio_venta + "</b>",
            icon: 'info',
            showConfirmButton: true,
            showCancelButton: false,
          });
          this.limpiarVenta();

          this.fecha_venta = "";
          this.tipo_venta = "";
          this.sat_forma_pago = "";
          this.registro_ventas = [];
        } else {
          Swal.fire({
            title: 'Venta Generada',
            text: 'La Venta fue generada pero no fue posible recuperar el numero de folio',
            icon: 'warning',
            showConfirmButton: false,
            showCancelButton: true,
          });
        }
      }
    }, error => {
      Swal.close();
      Swal.fire({
        title: 'Verificando Información',
        // html: 'Validando..',// add html attribute if you want or remove
        text: 'No fue posible procesar la información',
        icon: 'error',
        showConfirmButton: false,
        showCancelButton: true,
      });
      console.error(error);
    })
  }

  getClaveUnidadFiltrado(): ClaveUnidadModel[] {
    let catalogo_claves_unidad: ClaveUnidadModel[] = [];
    if(true){return catalogo_claves_unidad;}
    if (!this.form_registro_venta.controls.sat_clave_unidad.errors && this.form_registro_venta.value.sat_clave_unidad.length >= 1) {
      this.catalogo_claves_unidad.forEach(unidad => {
        if (unidad.descripcion != null && unidad.clave_unidad != null) {
          let valor_compara = this.form_registro_venta.value.sat_clave_unidad.toLocaleLowerCase();
          if (unidad.clave_unidad.toLocaleLowerCase().includes(valor_compara) || unidad.descripcion.toLocaleLowerCase().includes(valor_compara)) {
            if (catalogo_claves_unidad.length <= 50) {
              catalogo_claves_unidad.push(unidad);
            }
          }
        }
      });
    }
    return catalogo_claves_unidad;

  }
  getClaveProductoervicioFiltrado(): ClaveProdServModel[] {
    let catalogo_claves_productos_servicios: ClaveProdServModel[] = [];
    if(true){return catalogo_claves_productos_servicios;}
    if (!this.form_registro_venta.controls.sat_claveprodserv.errors && this.form_registro_venta.value.sat_claveprodserv.length >= 3) {
      this.catalogo_claves_productos_servicios.forEach(productoservicio => {
        if (productoservicio.descripcion != null && productoservicio.clave_producto_servicio != null) {

          let valor_compara = this.form_registro_venta.value.sat_claveprodserv.toLocaleLowerCase();
          let campo_filtro = productoservicio.clave_producto_servicio.toLocaleLowerCase();

          if (isNaN(Number(this.form_registro_venta.value.sat_claveprodserv))) {
            campo_filtro = productoservicio.descripcion.toLocaleLowerCase();
          }
          if (campo_filtro.includes(valor_compara)) {
            if (catalogo_claves_productos_servicios.length <= 50) {
              catalogo_claves_productos_servicios.push(productoservicio);
            }
          }
        }
      });
    }
    return catalogo_claves_productos_servicios;

  }

  getError(controlName: string): string {
    let error = this.funcionesServ.getError(this.form_registro_venta, controlName);
    console.log(error, 'erro de campo invalido forma pago');
    
    return error
  }

}
