import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { StorageService } from '../services/portal/storage.service';

@Injectable({
  providedIn: 'root'
})
export class AuthorizatedGuard implements CanActivate {

  constructor(private router: Router,
    private storageService: StorageService) { }

  canActivate(): boolean {
    if (this.storageService.isAuthenticated()) {
      return true;
    } else {
      this.router.navigate(['login'], { });
      return false;
    }
  }

}
