import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './administrativo/login/login.component';
import { LogoutComponent } from './administrativo/login/logout/logout.component';
import { InicioComponent } from './cliente/inicio/inicio.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { OrdersRoutingModule } from './orders/orders-routing.module';

const routes: Routes = [
 
  { path: '', component: InicioComponent },
  { path: 'login', component: LoginComponent },
  { path: 'logout', component: LogoutComponent },
  { path: 'cliente', loadChildren: () => import('./cliente/cliente.module').then(m => m.ClienteModule) },
  { path: 'administrativo', loadChildren: () => import('./administrativo/administrativo.module').then(m => m.AdministrativoModule) },
  //{ path: 'orders', loadChildren: () => import('./orders/orders.module').then(m => m.OrdersModule) }
  { path: 'orders', redirectTo:'orders' },
  {path:'**', component:NotFoundComponent},
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    OrdersRoutingModule
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
