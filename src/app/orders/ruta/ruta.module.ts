import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RutaRoutingModule } from './ruta-routing.module';
import { RutaComponent } from './ruta.component';


@NgModule({
  declarations: [RutaComponent],
  imports: [
    CommonModule,
    RutaRoutingModule
  ]
})
export class RutaModule { }
