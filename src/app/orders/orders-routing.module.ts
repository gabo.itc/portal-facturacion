import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OrdersComponent } from './orders.component';
import { RutaComponent } from './ruta/ruta.component';

const routes: Routes = [
  {
    path: 'orders',
    component: OrdersComponent,
    children: [
      // { path: 'ruta', loadChildren: () => import('./ruta/ruta.module').then(m => m.RutaModule) }
      { path: 'ruta', component: RutaComponent }
    ]
  }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrdersRoutingModule { }
