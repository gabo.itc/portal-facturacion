import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class FuncionesService {

  constructor() { }

  getError(form: FormGroup, controlName: string): string {
    let error = '';
    const control = form.get(controlName);
    if (control?.touched && control?.errors != null) {      
      if (control?.hasError('maxlength')) {
        error = `${controlName} demasiado largo`;
      }
      if (control?.hasError('pattern')) {
        error = `Formato de ${controlName} incorrecto`;
      }
      if (control?.hasError('minlength')) {
        error = `${controlName} demasiado corto`;
      }
      if (control?.hasError('required')) {
        error = `Campo ${controlName} obligatorio`;
      }
      if (control?.hasError('email')) {
        error = `Formato de ${controlName} incorrecto`;
      }
    }
    return error;
  }

}
