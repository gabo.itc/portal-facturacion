import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CatalogoFacturacionService {

  constructor(private http: HttpClient) { }

  usoCFDI(body: object): Observable<any> {
    return this.http.post(`${environment.webServiceFacturacion}/catalogo/uso-cfdi`, {})
      .pipe(
        map(resp => {
          return resp;
        }
        )
      );
  }

  regimenFiscal(body: object): Observable<any> {
    return this.http.post(`${environment.webServiceFacturacion}/catalogo/regimen-fiscal`, {})
      .pipe(
        map(resp => {
          return resp;
        }
        )
      );
  }

  solicitarFactura(body: object): Observable<any> {
    return this.http.post(`${environment.webServiceFacturacion}/facturacion/timbrar`, {})
      .pipe(
        map(resp => {
          return resp;
        }
        )
      );
  }

}
