import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ConceptosInterface } from 'src/app/models/conceptos.interface';


@Injectable({
  providedIn: 'root'
})
export class VentasService {
  constructor(private http: HttpClient) { }

  // conceptos:ConceptosInterface={};

  ventaConsultaFolio(dataForm: object): Observable<any> {
    let body = {}
    Object.assign(body, dataForm);
    return this.http.post(`${environment.webServicePortal}/venta/consulta`, body)
      .pipe(
        map(resp => {
          return resp;
        }
        )
      );
  }

  ventaConsultaFiltro(body: object): Observable<any> {
    return this.http.post(`${environment.webServicePortal}/venta/consulta/filtro`, body)
      .pipe(
        map(resp => {
          return resp;
        }
        )
      );
  }

  ventaRegistro(body: object): Observable<any> {
    return this.http.post(`${environment.webServicePortal}/venta/registro`, body)
      .pipe(
        map(resp => {
          return resp;
        }
        )
      );
  }

  ventaCancelar(id: string): Observable<any> {
    return this.http.post(`${environment.webServicePortal}/venta/cancelar/${id}`, {})
      .pipe(
        map(resp => {
          return resp;
        }
        )
      );
  }

  ventaCancelarConcepto(id: string, id_venta: string): Observable<any> {
    return this.http.post(`${environment.webServicePortal}/venta/cancelar/${id}`, { id_venta })
      .pipe(
        map(resp => {
          return resp;
        }
        )
      );
  }
  
  ventaTimbrar(body: object): Observable<any> {
    return this.http.post(`${environment.webServiceFacturacion}/venta/timbrar`, body)
      .pipe(
        map(resp => {
          return resp;
        }
        )
      );
  }

}
