import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CatalogoPortalService {

  constructor(private http: HttpClient) { }

  tipoVenta(body: object): Observable<any> {
    return this.http.post(`${environment.webServicePortal}/catalogo/tipo-venta`, body)
      .pipe(
        map(resp => {
          return resp;
        }
        )
      );
  }

  tipoPlataforma(body: object): Observable<any> {
    return this.http.post(`${environment.webServicePortal}/catalogo/plataforma`, body)
      .pipe(
        map(resp => {
          console.log("resp");
          console.log(resp);
          
          return resp;
        }
        )
      );
  }

}
