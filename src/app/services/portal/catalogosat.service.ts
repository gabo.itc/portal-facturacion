import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CatalogoSATService {

  constructor(private http: HttpClient) { }
  
  formaPago(body: object): Observable<any> {
    return this.http.post(`${environment.webServicePortal}/catalogo/forma-pago`, {})
      .pipe(
        map(resp => {
          console.log("formapago");
          console.log(resp);
          
          return resp;
        }
        )
      );
  }

  unidad(body: object): Observable<any> {
    return this.http.post(`${environment.webServicePortal}/catalogo/unidad`, body)
      .pipe(
        map(resp => {
          return resp;
        }
        )
      );
  }
  
  producto_servicio(body: object): Observable<any> {
    return this.http.post(`${environment.webServicePortal}/catalogo/producto-servicio`, body)
      .pipe(
        map(resp => {
          return resp;
        }
        )
      );
  }

}
