import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Session } from 'src/app/models/session.model';
import { User } from 'src/app/models/user.model';

@Injectable({
  providedIn: 'root'
})
export class StorageService {
  private localStorageService;
  private currentSession: User | null;

  constructor(private router: Router) {
    this.localStorageService = localStorage;
    this.currentSession = this.loadSessionData();
    // this.currentSession = null;
  }

  setCurrentSessions(session: User | any): void {
    this.currentSession = session;
    for (var property in session) {
      this.localStorageService.setItem(property, session[property]);
    }
  }

  loadSessionData(): User | null {
    let user: User | any = new User;
    for (var property in new User) {
      user[property] = this.localStorageService.getItem(property);
    }
    console.log(user);

    return user;
    // return (sessionStr) ? <User>JSON.parse(sessionStr) : null;
  }

  getCurrentSession(): User | null {
    return this.currentSession;
  }

  removeCurrentSession(): void {
    let session: User | any = this.currentSession;
    for (var property in session) {
      this.localStorageService.removeItem(property);
    }
    console.log(this.currentSession);
    
    this.currentSession = null;
    console.log(this.currentSession);
  }

  getCurrentUser(): string | null {
    var session: User | null = this.getCurrentSession();
    return (session && session.usuario_nombre) ? session.usuario_nombre : null;
  };

  isAuthenticated(): boolean {
    return (this.getCurrentUser() != null) ? true : false;
  };

  getCurrentToken(): string | null {
    var session = this.getCurrentSession();
    return (session && session.usuario_clave) ? session.usuario_clave : null;
  };

  logout(): void {
    this.removeCurrentSession();
    this.router.navigate(['/login']);
  }

}
