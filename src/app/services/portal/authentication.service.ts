import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { User, WSPortalResponse } from 'src/app/models/user.model';
import { Router } from '@angular/router';
import { StorageService } from './storage.service';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  private userSubject: BehaviorSubject<User | null>;
  public user: Observable<User | null>;

  constructor(
    private storage: StorageService,
    private router: Router,
    private http: HttpClient
  ) {
    let usuario: string | null = localStorage.getItem('user');
    this.userSubject = new BehaviorSubject<User | null>(
      (usuario != null ? JSON.parse(usuario) : null));
    this.user = this.userSubject.asObservable();
  }

  public get userValue(): User | null {
    return this.userSubject.value;
  }

  login(usuario_nombre: string, usuario_clave: string) {
    return this.http.post<User>(`${environment.webServicePortal}/login`, { usuario_nombre, usuario_clave })
      .pipe(map(response => {
        this.userSubject.next(response);
        this.storage.setCurrentSessions(response);
        this.router.navigate(['/administrativo']);

        return response;
      }));
  }

  logout() {
    // remove user from local storage and set current user to null
    localStorage.removeItem('user');
    this.userSubject.next(null);
    this.router.navigate(['/login']);
  }
}
