import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { FacturaModel } from 'src/app/models/factura.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FacturacionService {

  constructor(private http: HttpClient) { }

  consultaFiltro(body: object): Observable<any> {
    return this.http.post(`${environment.webServicePortal}/facturacion/consulta/filtro`, body)
      .pipe(
        map(resp => {
          return resp;
        }
        )
      );
  }

  ventaCancelarConcepto(id: any, id2: any): Observable<any> {
    return this.http.post(`${environment.webServicePortal}/facturacion/consulta`, {})
      .pipe(
        map(resp => {
          return resp;
        }
        )
      );
  }

  facturaCancelar(factura: FacturaModel): Observable<any> {
    return this.http.post(`${environment.webServicePortal}/facturacion/cancelar/`, factura)
      .pipe(
        map(resp => {
          return resp;
        }
        )
      );
  }

}
