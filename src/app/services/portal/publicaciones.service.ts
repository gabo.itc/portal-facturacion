import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PublicacionesService {

  constructor(private http: HttpClient) { }

  publicacionRegistro(body: object): Observable<any> {
    return this.http.post(`${environment.webServicePortal}/publicacion/registro`, body)
      .pipe(
        map(resp => {
          return resp;
        }
        )
      );
  }

}
