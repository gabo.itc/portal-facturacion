
export interface ConceptosInterface {
  Concepto: ConceptoModel;
}

export interface ConceptoModel {
  Cantidad: string;
  Descripcion: string;
  ClaveProdServ: string;
  NoIdentificacion: string;
  ClaveUnidad: string;
  Unidad: string;
  ValorUnitario: number;
  Importe: number;
}

export interface ConceptoRegistroModel {
  cantidad: number;
  concepto: string;
  identificador: string;
  unidad: string;
  sat_clave_unidad: string;
  sat_claveprodserv: string;
  sat_forma_pago: string;
  sat_objeto_impuesto: string; //clave sat para el tipo de impuesto
  precio_unitario: number;//valor unitario
  descuento: number;//importe de descuento antes de claculo del iva
  importe: number;//importe sin iva
  aplica_iva: boolean;// true si aplica iva, false en caso contrario
  incluye_iva: boolean;// true si el importe ya incluye IVA, false en caso contrario
  tasa_iva: number;//tasa del iva cobrado en caso de que no aplique iva es null
  importe_iva: number;//importe del iva del producto
}