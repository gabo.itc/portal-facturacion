export class ClaveProdServModel {
    clave_producto_servicio: string | null = null;
    descripcion: string | null = null;
    estimulo_fiscal: string | null = null;
    incluir_ieps_trasladado: string | null = null;
    incluir_iva_trasladado: string | null = null;
    palabras_similares: string | null = null;
}

export class ClaveUnidadModel {
    clave_unidad: string | null = null;
    descripcion: string | null = null;
}
