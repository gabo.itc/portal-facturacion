export class User {
    pk_usuario: number | null = null;
    nombre: string | null = null;
    apellido_paterno: string | null = null;
    apellido_materno: string | null = null;
    usuario_nombre: string | null = null;
    usuario_clave: string | null = null;
    fk_cliente: number | null = null;
}
export interface WSPortalResponse {
    error: boolean;
    message: string;
    body: any;
}