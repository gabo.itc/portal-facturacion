
export interface PublicacionesInterface {
    Publicacion: PublicacionModel;
}

export interface PublicacionModel {
    Cantidad: string;
    Descripcion: string;
    ClaveProdServ: string;
    NoIdentificacion: string;
    ClaveUnidad: string;
    Unidad: string;
    ValorUnitario: number;
    Importe: number;
}

export interface PublicacionRegistroModel {
    fecha_publicacion: string;
    plataforma_acronimo: string;
    titulo: string;
    identificador: string;
    unidad: string;
    sat_clave_unidad: string;
    sat_claveprodserv: string;
    precio_unitario: number;//valor unitario
    importe: number;//importe sin iva
    aplica_iva: boolean;// true si aplica iva, false en caso contrario
    incluye_iva: boolean;// true si el importe ya incluye IVA, false en caso contrario
    tasa_iva: number;//tasa del iva cobrado en caso de que no aplique iva es null
    importe_iva: number;//importe del iva del producto
    link: string;//link para acceder a la publicación
}