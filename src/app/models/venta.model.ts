export interface VentaModel {
    id_venta: string;
    fecha_venta: string;
    folio_venta: string;
    cantidad: number;
    concepto: string;
    sat_clave_producto_servicio: string;
    numero_identificador: string;
    sat_clave_unidad: string;
    unidad: string;
    precio_unitario: number;
    importe_venta: number;
    aplica_iva: string;
    tasa_iva: number;
    importe_iva: number;
    importe_total: number;
    descuento: number;
}