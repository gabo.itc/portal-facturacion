export interface FacturaModel {
    id_factura: string;
    tipo_comprobante: string;
    fecha_generada: Date;
    serie: string;
    folio: string;
    rfc: string;
    razon_social: string;
    estatus_factura: string;
    estatus_cancelacion: string;
    xml_factura: string;
    xml_acuse: string;
    uuid: string;
    sello_sat_8: string;
    importe_total: number;
    importe_venta: number;
    aplica_iva: string;
    tasa_iva: number;
    importe_iva: number;
    descuento: number;
}