export const environment = {
  production: true,
  // webServicePortal: "https://portal.tudescargamasiva.com/ws/WebServicesFacturacion/public",
  // webServicePortal: "https://ws.portal.tudescargamasiva.com",
  // webServiceFacturacion: "https://portal.tudescargamasiva.com/ws/WebServicesFacturacion/public",
  webServicePortal: "http://ws.portal.descargasat.com",
  webServiceFacturacion: "http://ws.portal.descargasat.com",
};
